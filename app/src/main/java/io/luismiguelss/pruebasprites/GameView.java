package io.luismiguelss.pruebasprites;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

public class GameView extends SurfaceView {

    private SurfaceHolder holder;
    private GameLoopThread gameLoopThread;
    private ArrayList<Sprite> sprites = new ArrayList<>();
    private ArrayList<TemporalSprite> temps = new ArrayList<TemporalSprite>();
    private long lastClick;

    private Bitmap bmpBlood;


    public GameView(Context context) {

        super(context);
        setBackgroundColor(Color.TRANSPARENT);
        gameLoopThread = new GameLoopThread(this);
        holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                gameLoopThread.setRunning(false);
                while (retry) {
                    try {
                        gameLoopThread.join();
                        retry = false;
                    } catch (InterruptedException e) { }
                }
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                createSprites();
                gameLoopThread.setRunning(true);
                gameLoopThread.start();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

        });
        bmpBlood = BitmapFactory.decodeResource(getResources(), R.drawable.blood);
    }

    private void createSprites() {
        sprites.add(createSprite(R.drawable.bad1));
        sprites.add(createSprite(R.drawable.bad1));
        sprites.add(createSprite(R.drawable.bad1));
    }

    private Sprite createSprite(int resource) {
        return new Sprite(this,BitmapFactory.decodeResource(getResources(),resource));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.WHITE);

        for (int i = temps.size() - 1; i >= 0; i--)
            temps.get(i).onDraw(canvas);

        for (Sprite s:sprites)
            s.onDraw(canvas);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (System.currentTimeMillis() - lastClick > 500) {
            lastClick = System.currentTimeMillis();

            synchronized (getHolder()) {
                for (int i = sprites.size()-1; i > -1; i--) {
                    Sprite sprite = sprites.get(i);
                    if (sprite.isCollition(event.getX(),event.getY())) {

                        // Play dying.mp3 sound

                        temps.add(new TemporalSprite(temps, this, event.getX(), event.getY(), bmpBlood));
                        sprites.remove(sprite);
                        break;
                    }
                }
            }
        }
        return true;
    }

}
